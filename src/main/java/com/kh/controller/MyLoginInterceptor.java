package com.kh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.kh.utils.Tools;

public class MyLoginInterceptor implements HandlerInterceptor {

	List<String> excludeUrls;

	

	public List<String> getExcludeUrls() {
		System.out.println("=============获取不被拦截的路由==============");
		return excludeUrls;
	}

	public void setExcludeUrls(List<String> excludeUrls) {
		System.out.println("=============设置不被拦截的路由==============");
		this.excludeUrls = excludeUrls;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		String router = request.getServletPath();
		String router = Tools.getRequestPath(request);
		
		if(excludeUrls.contains(router)) {
			return true;
		}
//		if(null ==UserUtils.getUser()) {
//			response.sendRedirect("loginController.do?goLogin");
//			return false;
//		}

		
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
