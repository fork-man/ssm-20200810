package com.kh.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.kh.pojo.HelloPojo;
import com.kh.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kh.utils.Tools;

@Controller
@RequestMapping("/hello")
public class HelloController {

    @Autowired
    HelloService sayHello;

    //	http://localhost:1234/learn/hello.do?sayHello&title=你好&error=0
    @RequestMapping(params = "sayHello")
    @ResponseBody
    public String sayHello(String title, int error) {

        HelloPojo hello = new HelloPojo();
        hello.setId(Tools.getRandom());
        hello.setTitle(title);
        hello.setContent("内容555555");
        sayHello.save(hello,error);
        return "555";
    }


}
