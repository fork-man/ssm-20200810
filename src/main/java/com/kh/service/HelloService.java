package com.kh.service;

import com.kh.dao.HelloDao;
import com.kh.pojo.HelloPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class HelloService {

    @Autowired
    HelloDao helloDao;

    public void save(HelloPojo hello, int error) {
        helloDao.save(hello);
        if (0 != error) {
            int i = 1 / 0;
        }
    }
}
