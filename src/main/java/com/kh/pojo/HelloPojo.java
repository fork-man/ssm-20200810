package com.kh.pojo;

import lombok.Data;

@Data
public class HelloPojo {
    private Long id;
    private String title;
    private String content;
}
