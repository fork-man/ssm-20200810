package com.kh.pojo;

public class AjaxJson {
	
	public static final int ok=1;
	public static final int error=-1;
	private int code;
	
	private String message;
	private String url;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	

}
