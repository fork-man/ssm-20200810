package com.kh.dao;

import java.util.List;

import com.kh.pojo.HelloPojo;
import com.kh.utils.MyBatisRepository;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

//@MyBatisRepository
@Mapper
@Repository
public interface HelloDao {
	List<HelloPojo> blogList(HelloPojo pojo);
	
	void save(HelloPojo hello);
}
