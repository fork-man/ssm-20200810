package com.kh.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class Tools {

	public static Long getRandom() {
		SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHmmss");
		String s = dateformat2.format(new Date());
		String randomString = s.substring(0, 10) + new java.util.Random().nextInt(100000000);
		return Long.parseLong(randomString);
	}

	/*
	 * 生成六位随机整数
	 */
	public static String getSixRandomNum() {
		String s = "";
		for (int i = 0; i < 6; i++) {
			int n;
			n = (int) (Math.random() * 10);
			s = s + n;
		}
		return s;
	}

	public static Float roundDown(Float source) {
		BigDecimal result = new BigDecimal(source);
		result = result.setScale(2, BigDecimal.ROUND_HALF_UP);
		Float f = result.floatValue();
		return f;
	}

	public static void main(String[] args) {
		System.out.println(roundDown(15.8000001907349f));
	}

	public static String getIp(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	public static String getRequestPath(HttpServletRequest request) {
		String requestPath = "";
		if (StringUtils.isNotEmpty(request.getQueryString())) {
			requestPath = request.getRequestURI() + "?" + request.getQueryString();
			if (requestPath.indexOf("&") > -1) {// 去掉其他参数
				requestPath = requestPath.substring(0, requestPath.indexOf("&"));
			}
			requestPath = requestPath.substring(request.getContextPath().length() + 1);// 去掉项目路径
		} else {
			requestPath = request.getRequestURI();
			requestPath = requestPath.substring(request.getContextPath().length() + 1);// 去掉项目路径
		}

		return requestPath;
	}
}
