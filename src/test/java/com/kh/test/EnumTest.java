package com.kh.test;

import com.kh.test.EnumTest.week;

public class EnumTest {
	public static enum week{
		MON("周一"),TUE("周二"),WED("周三");
		String weekName;

		private week(String weekName) {
			this.weekName = weekName;
		}

		public String getWeekName() {
			return weekName;
		}

		public void setWeekName(String weekName) {
			this.weekName = weekName;
		}
		
	}
	public static void main(String[] args) {
		week[] values = week.values();
		for (int i = 0; i < values.length; i++) {
			week week2 = values[i];
			System.out.println(week2.getWeekName());
		}
	}

}
